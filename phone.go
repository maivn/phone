package phone

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strings"
)

const (
	DefaultCountryCode = "+7"
)

type Phone struct {
	CountryCode string `json:"country_code"`
	CityCode    string `json:"city_code"`
	Number      string `json:"number"`
	IsMobile    bool   `json:"is_mobile"`
	Raw         string `json:"raw"`
}

func Parse(raw string) (*Phone, error) {
	phone := Phone{Raw: raw}

	var re = regexp.MustCompile(`[^0-9]`)
	raw = re.ReplaceAllString(raw, "")

	switch l := len(raw); true {
	case l < 10:
		return nil, errors.New("Номер телефона некорректен. ")
	case l == 10:
		phone.CountryCode = DefaultCountryCode
	case l > 10:
		countryCode := raw[0 : l-10]
		if countryCode == "7" || countryCode == "8" || countryCode == "+8" {
			countryCode = "+7"
		}

		if string(countryCode[0]) != "+" {
			countryCode = "+" + countryCode
		}
		phone.CountryCode = countryCode
		raw = raw[l-10:]
	}

	codes, err := getCodes()
	if err != nil {
		return nil, err
	}

	if cityCodes, has := codes[phone.CountryCode]; has {
		for cityCode, isMobile := range cityCodes {
			if strings.HasPrefix(raw, cityCode) {
				phone.CityCode = cityCode
				phone.Number = strings.TrimPrefix(raw, cityCode)
				phone.IsMobile = isMobile
				return &phone, nil
			}
		}
	}

	return nil, errors.New("Номер телефона некорректен. ")
}

func getCodes() (map[string]map[string]bool, error) {
	jsonFile, err := os.Open(fmt.Sprintf("%s\\%s", os.Getenv("GOPATH"), "src\\gitlab.com\\maivn\\phone\\codes.json"))
	if err != nil {
		return nil, err
	}
	defer jsonFile.Close()

	bytes, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	codes := make(map[string]map[string]bool)
	err = json.Unmarshal(bytes, &codes)
	if err != nil {
		return nil, err
	}
	return codes, nil
}

func (p *Phone) FullNum() string {
	return p.CountryCode + p.CityCode + p.Number
}

func (p *Phone) Num() string {
	return p.CityCode + p.Number
}
